import React from "react";
import CircularProgress from "@mui/joy/CircularProgress";
import Box from "@mui/joy/Box";

const Loader = () => {
  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      height="80vh"
      width="100%"
      sx={{}}
    >
      <CircularProgress size="lg" />
    </Box>
  );
};

export default Loader;
