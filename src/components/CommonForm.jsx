import React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import { useState } from "react";
import { fontWeight, padding } from "@mui/system";

const CommonForm = ({ onFormSubmit ,formName}) => {
  const [formInput, setFormInput] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (formInput.trim().length > 0) {
      onFormSubmit(formInput);
    }
    setFormInput("");
  };

  return (
    <div>
      <form style={{ display: "flex" }} onSubmit={handleSubmit}>
        <Box display={"flex"} pl={"0.5rem"}>
          <TextField
            required
            label="title"
            variant="standard"
            name="title"
            placeholder="enter card title"
            value={formInput}
            onChange={(event) => {
              setFormInput(event.target.value);
            }}
          />
          <Button sx={style} type="submit" >
            + {formName}
          </Button>
        </Box>
      </form>
    </div>
  );
};

export default CommonForm;

const style = {
  backgroundColor: "inherit",
  color: "rgba(0, 0, 0, 0.6)",
  fontSize: "0.7rem",
  width: "auto",
  fontWeight:'bold',
  padding:0,
};
