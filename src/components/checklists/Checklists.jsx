import React from "react";
import ModalLayout from "../ModalLayout";
import Box from "@mui/material/Box";
import { useState, useEffect } from "react";
import { getChecklists } from "../../apiCalling";
import CheckItems from "./CheckItems";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import { deleteChecklistApiCall } from "../../apiCalling";
import { createChecklistApiCall } from "../../apiCalling";
import toast from "react-hot-toast";
import CommonForm from "../CommonForm";

const Checklists = ({ cardName, cardId }) => {
  const [checklists, setChecklists] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const checklistsData = await getChecklists(cardId);
        setChecklists(checklistsData);
      } catch (error) {
        toast.error(`Error in fetching checklists ${error.message}`);
      }
    };
    fetchData();
  }, []);

  const onCreateChecklist = async (checkListName) => {
    try {
      const newChecklistData = await createChecklistApiCall(
        cardId,
        checkListName
      );
      setChecklists([newChecklistData, ...checklists]);
      toast.success("checklist successfully created");
    } catch (error) {
      toast.error(error.message);
    }
  };

  const handleDeleteChecklist = async (checklistId) => {
    const ChecklistsCopy = checklists.map((checklist) => {
      return { ...checklist, checkItems: [...checklist.checkItems] };
    });
    const updatedCheckLists = ChecklistsCopy.filter((checklist) => {
      return checklist.id !== checklistId;
    });
    try {
      await deleteChecklistApiCall(checklistId);
      setChecklists(updatedCheckLists);
      toast.success("Checklist successfully deleted");
    } catch (error) {
      toast.error(`error in deleting checklist ${error.message}`);
    }
  };

  const handleCreateCheckItem = (checkItem, checklistId) => {
    const updatedChecklist = checklists.map((checklist) => {
      if (checklist.id === checklistId) {
        return {
          ...checklist,
          checkItems: [...checklist.checkItems, checkItem],
        };
      }
      return { ...checklist, checkItems: [...checklist.checkItems] };
    });
    setChecklists(updatedChecklist);
  };

  const handleClickCheckItem = async (status, checklistId, checkItemId) => {
    const updatedChecklists = checklists.map((checklist) => {
      if (checklist.id === checklistId) {
        const checkItemArr = checklist.checkItems.map((item) => {
          if (item.id === checkItemId && status) {
            return { ...item, state: "complete" };
          } else if (item.id === checkItemId && status === false) {
            return { ...item, state: "incomplete" };
          } else {
            return item;
          }
        });
        return { ...checklist, checkItems: checkItemArr };
      } else {
        return checklist;
      }
    });
    setChecklists(updatedChecklists);
  };

  const handleDeleteCheckItem = (checklistId, checkItemId) => {
    const updatedChecklists = checklists.map((checklist) => {
      if (checklist.id === checklistId) {
        const checkItemArr = checklist.checkItems.filter((item) => {
          return item.id != checkItemId;
        });
        return { ...checklist, checkItems: checkItemArr };
      } else {
        return checklist;
      }
    });
    setChecklists(updatedChecklists);
  };

  return (
    <>
      <ModalLayout name={cardName.toUpperCase()}>
        <CommonForm onFormSubmit={onCreateChecklist} formName='Add Checklist'/>
        {checklists.map((checklist) => (
          <Box key={checklist.name}>
            <Box
              key={checklist.id}
              display={"flex"}
              alignItems={"center"}
              justifyContent={"space-between"}
            >
              <h3>{checklist.name}</h3>
              <IconButton
                aria-label="delete"
                onClick={() => handleDeleteChecklist(checklist.id)}
                sx={{ marginRight: "10px" }}
              >
                <DeleteIcon />
              </IconButton>
            </Box>
            <CheckItems
              checkItems={checklist.checkItems}
              checklist={checklist}
              onCreateCheckItem={handleCreateCheckItem}
              onClickCheckItem={handleClickCheckItem}
              onDeleteCheckItem={handleDeleteCheckItem}
              cardId={cardId}
            />
          </Box>
        ))}
      </ModalLayout>
    </>
  );
};

export default Checklists;
