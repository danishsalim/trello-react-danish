import React from "react";
import { useState } from "react";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { Button, Box, TextField } from "@mui/material";
import { createCheckItemApiCall } from "../../apiCalling";
import { deleteCheckItemApiCall } from "../../apiCalling";
import { toggleCheckBoxApiCall } from "../../apiCalling";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import toast from "react-hot-toast";
import CommonForm from "../CommonForm";

const CheckItems = ({
  checkItems,
  checklist,
  onCreateCheckItem,
  onClickCheckItem,
  onDeleteCheckItem,
  cardId,
}) => {

  const [loading, setloading] = useState(false);

  const createCheckItem = async (formInput) => {
    try {
      const newCheckItem = await createCheckItemApiCall(
        checklist.id,
        formInput
      );
      onCreateCheckItem(newCheckItem, checklist.id);
    } catch (error) {
      toast.error(error.message);
    }
  };

  const toggleCheckbox = async (cardId, checkItemId, state) => {
    try {
      const response = await toggleCheckBoxApiCall(cardId, checkItemId, state);
    } catch (error) {
      toast.error(error.message);
    }
  };

  const handleChange = (event, checkItemId) => {
    let state = event.target.checked ? "complete" : "incomplete";
    toggleCheckbox(cardId, checkItemId, state);
    onClickCheckItem(event.target.checked, checklist.id, checkItemId);
  };

  const handleDelete = async (checkItemId) => {
    try {
      setloading(true);
      const response = await deleteCheckItemApiCall(checklist.id, checkItemId);
      toast.success("item deleted");
      onDeleteCheckItem(checklist.id, checkItemId);
    } catch (error) {
      toast.error("error.message");
    } finally {
      setloading(false);
    }
  };

  const handleFormSubmit = (formInput) => {
    createCheckItem(formInput);
  };

  return (
    <FormGroup>
      {loading ? (
        <img src="../giphy.webp" alt="" height={"20%"} width={"20%"} />
      ) : (
        checkItems.map((checkItem) => (
          <Box
            display={"flex"}
            justifyContent={"space-between"}
            key={checkItem.id}
          >
            <FormControlLabel
              key={checkItem.id}
              sx={checkItem.state === "complete" ? { textDecoration: 'line-through' } : {}}
              control={
                <Checkbox
                  onChange={(e) => handleChange(e, checkItem.id)}
                  checked={checkItem.state === "complete"}
                />
              }
              label={checkItem.name}
            />
            <IconButton
              aria-label="delete"
              onClick={() => handleDelete(checkItem.id)}
              sx={{ marginRight: "10px" }}
            >
              <DeleteIcon />
            </IconButton>
          </Box>
        ))
      )}

      <CommonForm  onFormSubmit={handleFormSubmit} formName='Add Item'/>
    </FormGroup>
  );
};

export default CheckItems;

const style = {
  backgroundColor: "inherit",
  color: "rgba(0, 0, 0, 0.6)",
  fontSize: "0.6rem",
  width: "50%",
  paddingTop: "7%",
};
