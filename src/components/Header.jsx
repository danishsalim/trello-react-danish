import * as React from "react";
import Box from "@mui/joy/Box";
import List from "@mui/joy/List";
import ListItem from "@mui/joy/ListItem";
import ListItemButton from "@mui/joy/ListItemButton";
import Home from "@mui/icons-material/Home";
import ListItemText from "@mui/material/ListItemText";
import { Link } from "react-router-dom";

export default function Header() {
  return (
    <Box
      component="nav"
      aria-label="My site"
      marginBottom={4}
      sx={{ flexGrow: 1 }}
    >
      <List role="menubar" orientation="horizontal">
        <ListItem role="none">
          <Link to="/">
            <ListItemButton role="menuitem" aria-label="Home">
              <Home />
            </ListItemButton>
          </Link>
        </ListItem>

        <ListItem role="none">
          {/* <ListItemButton role="menuitem" component="a" href="#horizontal-list">
            Boards
          </ListItemButton> */}
          <ListItemText primary="Trello" />
        </ListItem>
      </List>
    </Box>
  );
}
