import React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import { useState } from "react";
import { createListApiCall } from "../../apiCalling";
import ModalForm from "../ModalForm";
import toast from "react-hot-toast";

const CreateListModal = ({ onCreateNewList, boardId }) => {
  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const createList = async (listName) => {
    try {
      const boardData = await createListApiCall(boardId, listName);
      onCreateNewList(boardData);
      toast.success(" List Successfully created!");
    } catch (error) {
      toast.error(`${error.message} !! list not created`);
    }
  };

  const handleFormSubmit = (listName) => {
    handleClose();
    createList(listName);
  };

  return (
    <div>
      <Button
        variant="contained"
        sx={{
          backgroundColor: "inherit",
          color: "rgba(0, 0, 0, 0.6)",
          "&:hover": {
            backgroundColor: "rgba(0, 0, 0, 0.08)",
          },
        }}
        onClick={() => {
          handleOpen();
        }}
      >
        + Add another List
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box>
          <ModalForm onFormSubmit={handleFormSubmit} name="Add List" />
        </Box>
      </Modal>
    </div>
  );
};

export default CreateListModal;
