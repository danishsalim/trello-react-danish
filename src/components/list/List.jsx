import * as React from "react";
import { useState } from "react";
import toast from "react-hot-toast";
import Loader from "../Loader";
import ListCards from "../listCard/ListCards";
import { deleteListApiCall } from "../../apiCalling";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import DeleteIcon from "@mui/icons-material/Delete";

export default function List({ name, listId, onDeleteList }) {
  const [loading, setLoading] = useState(false);
  
  const handleDeleteList = async () => {
    try {
      setLoading(true);
      const response = await deleteListApiCall(listId);
      onDeleteList(listId);
      toast.success("list deleted successfully");
    } catch (error) {
      toast.error(`${error.message} list not deleted`);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Card
      sx={{
        width: 275,
        padding: "1%",
      }}
    >
      {loading ? (
        <Loader />
      ) : (
        <>
          <CardContent
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "start",
            }}
          >
            <Typography sx={{ mb: 1.5 }} color="text.secondary">
              {name}
            </Typography>
            <DeleteIcon onClick={() => handleDeleteList()} />
          </CardContent>
          <ListCards listId={listId} />
        </>
      )}
    </Card>
  );
}
