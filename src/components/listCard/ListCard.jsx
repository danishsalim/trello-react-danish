import * as React from "react";
import { useState } from "react";
import Checklists from "../checklists/Checklists";
import { deleteCardApiCall } from "../../apiCalling";
import Card from "@mui/material/Card";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import ButtonGroup from "@mui/material/ButtonGroup";
import toast from "react-hot-toast";

const ListCard = ({ cardName, cardId, onCardDelete }) => {
  const [loading, setLoading] = useState(false);

  const handleDelete = async () => {
    try {
      setLoading(true);
      await deleteCardApiCall(cardId);
      onCardDelete(cardId);
      toast.success("Card deleted successfully");
    } catch (error) {
      toast.error(error.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Card
      sx={{
        width: "100%",
        height: "50px",
        display: "flex",
        justifyContent: "space-around",
        alignItems: "flex-start",
        marginBottom: "5px",
      }}
    >
      {loading ? (
        <img src="../giphy.webp" alt="loading..." height={'80%'} width={'60%'}/>
      ) : (
        <ButtonGroup
          variant=""
          fullWidth
          sx={{
            textAlign: "left",
            "&:hover": {
              backgroundColor: "rgba(0, 0, 0, 0.08)",
            },
          }}
        >
          <Checklists cardName={cardName} cardId={cardId} />
          <IconButton
            aria-label="delete"
            onClick={() => handleDelete()}
            sx={{ marginRight: "10px" }}
          >
            <DeleteIcon />
          </IconButton>
        </ButtonGroup>
      )}
    </Card>
  );
};

export default ListCard;
