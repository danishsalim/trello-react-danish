import React from "react";
import { useState, useEffect } from "react";
import ListCard from "./ListCard";
import { createCardApiCall, getCardsApiCall } from "../../apiCalling";
import toast from "react-hot-toast";
import CommonForm from "../CommonForm";

const ListCards = ({ listId }) => {
  const [listCards, setListCards] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchCards = async () => {
      try {
        const cardsData = await getCardsApiCall(listId);
        setListCards(cardsData);
      } catch (error) {
        toast.error("error in fetching cards data");
      } finally {
        setLoading(false);
      }
    };
    fetchCards();
  }, []);

  const handleCardDelete = (cardId) => {
    let updatedCards = listCards.filter((card) => card.id != cardId);
    setListCards(updatedCards);
  };

  const handleFormSubmit = async (formInput) => {
    let cardName = formInput;
    try {
      const card = await createCardApiCall(listId, cardName);
      setListCards([...listCards, card]);
    } catch (error) {
      toast.error(error.message);
    }
  };

  return (
    <>
      {loading ? (
        <img src="../giphy.webp" alt="" />
      ) : (
        listCards.map((card) => (
          <ListCard
            key={card.id}
            cardName={card.name}
            cardId={card.id}
            onCardDelete={handleCardDelete}
          />
        ))
      )}

      <CommonForm onFormSubmit={handleFormSubmit} formName="Add Card" />
    </>
  );
};

export default ListCards;
