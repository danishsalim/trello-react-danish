import React from "react";
import { useState } from "react";
import { Button, TextField, Box } from "@mui/material";

const ModalForm = ({onFormSubmit, name}) => {
  const [inputValue, setInputValue] = useState("");
  
  const handleChange = (event) => {
    setInputValue(event.target.value);
  };
  
  const handleFormSubmit=(event)=>{
    event.preventDefault()
    if(inputValue.trim().length > 0){
      onFormSubmit(inputValue)
    }
    
  }

  return (
    <Box component="form" onSubmit={handleFormSubmit} sx={style}>
      <TextField
        label={name}
        value={inputValue}
        onChange={handleChange}
        required
      />
      <Button type="submit" variant="contained" color="primary" sx={{ m: 2 }}>
        {name}
      </Button>
    </Box>
  );
};

export default ModalForm;

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};