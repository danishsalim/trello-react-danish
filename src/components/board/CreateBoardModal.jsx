import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import { useState } from "react";
import { createBoardApiCall } from "../../apiCalling";
import ModalForm from "../ModalForm";
import toast from "react-hot-toast";


export default function CreateBoardModal({ onCreateBoard }) {
  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const createBoard = async (boardName) => {
    try {
      const boardData = await createBoardApiCall(boardName);
      onCreateBoard(boardData)
      toast.success("Successfully created!");
    } catch (error) {
      toast.error(`Error in creating new Board ${error.message} !!`);
    } 
  };

  const handleFormSubmit = (boardName) => {
    handleClose();
    createBoard(boardName);
  };

  return (
    <>
      <Button
        variant="contained"
        sx={{
          backgroundColor: "inherit",
          color: "rgba(0, 0, 0, 0.6)",
          "&:hover": {
            backgroundColor: "rgba(0, 0, 0, 0.08)",
          },
        }}
        onClick={handleOpen}
      >
        Create New Board
      </Button>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box>
          <ModalForm onFormSubmit={handleFormSubmit} name="Add Board" />
        </Box>
      </Modal>
    </>
  );
}
