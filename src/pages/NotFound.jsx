import React from "react";
import { Link } from "react-router-dom";
import Box from "@mui/material/Box";
import { Button } from "@mui/material";

const NotFound = ({errorMessage}) => {
  return (
    <Box
      display="flex"
      alignItems="center"
      flexDirection="column"
      width={'100%'}
      gap={4}
      p={2}
      sx={{}}
    >
      <h1>Error !!!</h1>
      <p>{errorMessage}</p>
      <Link to="/">
        <Button variant="contained"> Go Back</Button>
      </Link>
    </Box>
  );
};

export default NotFound;
