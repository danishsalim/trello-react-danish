import React from "react";
import { useState, useEffect } from "react";
import Board from "../components/board/Board";
import { getBoards } from "../apiCalling";
import Box from "@mui/material/Box";
import { Link } from "react-router-dom";
import CreateBoardModal from "../components/board/CreateBoardModal";
import Loader from "../components/Loader";
import Typography from "@mui/material/Typography";
import NotFound from "../pages/NotFound";

const Home = () => {
  const [boards, setBoards] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      try {
        const boardsData = await getBoards();
        setBoards(boardsData);
      } catch (error) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  const handleCreateBoard = (board) => {
    setBoards([board, ...boards]);
  };

  return (
    <>
      {error ? (
        <NotFound errorMessage={error.message} />
      ) : (
        <>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
            pl={5}
            pr={5}
            pb={5}
          >
            <Typography sx={{ mb: 1.5 }} color="text.secondary">
              Boards
            </Typography>
            <CreateBoardModal onCreateBoard={handleCreateBoard} />
          </Box>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="start"
            gap={4}
            p={2}
            flexWrap={"wrap"}
            pl={5}
            pb={5}
            sx={{
              flexFlow: "wrap",
            }}
          >
            {loading ? (
              <Loader />
            ) : (
              <>
                {boards.map((board) => (
                  <Link
                    to={`/boards/${board.id}`}
                    key={board.id}
                    className="link"
                  >
                    <Board name={board.name} />
                  </Link>
                ))}
              </>
            )}
          </Box>
        </>
      )}
    </>
  );
};

export default Home;
