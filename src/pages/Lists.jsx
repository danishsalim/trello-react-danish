import { getLists } from "../apiCalling";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import List from "../components/list/List";
import Loader from "../components/Loader";
import NotFound from "./NotFound";
import CreateListModal from "../components/list/CreateListModal";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

const Lists = () => {
  const [lists, setLists] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  const { id } = useParams();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await getLists(id);
        setLists(response.data);
      } catch (error) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  const handleCreateNewList = (list) => {
    setLists([list, ...lists]);
  };

  const handleDeleteList = (listId) => {
    let updatedLists = lists.filter((list) => list.id != listId);
    setLists(updatedLists);
  };

  if (error) {
    return <NotFound errorMessage={error.message} />;
  }

  return (
    <>
    <Typography sx={{ ml: 6,mb:4 }} color="text.secondary">
        Lists
      </Typography>
      {loading ? (
        <Loader />
      ) : (
        <Box
          display="flex"
          alignItems="start"
          justifyContent="start"
          gap={4}
          p={2}
          flexWrap={"wrap"}
          ml={4}
        >
          
          {lists.map((list) => (
            <List
              name={list.name}
              listId={list.id}
              onDeleteList={handleDeleteList}
              key={list.id}
            />
          ))}
          <CreateListModal onCreateNewList={handleCreateNewList} boardId={id} />
        </Box>
      )}
    </>
  );
};

export default Lists;
