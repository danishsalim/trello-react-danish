import axios from "axios";

export const createBoardApiCall = async (boardName) => {
  const url = `https://api.trello.com/1/boards/?name=${boardName}&key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A`;
  const response = await axios({
    method: "post",
    url: url,
  });
  return response.data;
};

export const getBoards = async () => {
  const url = `https://api.trello.com/1/members/me/boards?key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A&fields=name,url`;
  const response = await axios.get(url);
  return response.data;
};

export const getLists = async (boardId) => {
  const url = `https://api.trello.com/1/boards/${boardId}/lists?key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A`;
  const response = await axios.get(url);
  return response;
};

export const createListApiCall = async (boardId, ListName) => {
  const url = `https://api.trello.com/1/lists?name=${ListName}&idBoard=${boardId}&key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A`;
  const response = await axios({
    method: "post",
    url: url,
  });
  return response.data;
};

export const deleteListApiCall = async (listId) => {
  const url = `https://api.trello.com/1/lists/${listId}/closed?key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A&value=true`;
  const response = await axios({
    method: "put",
    url: url,
  });
  return response.data;
};

export const getCardsApiCall = async (listId) => {
  const url = `https://api.trello.com/1/lists/${listId}/cards?key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A`;
  const response = await axios({
    method: "get",
    url: url,
  });
  return response.data;
};

export const createCardApiCall = async (listId, cardName) => {
  const url = `https://api.trello.com/1/cards?idList=${listId}&key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A&name=${cardName}`;
  try {
    const response = await axios({
      method: "post",
      url: url,
    });
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export const deleteCardApiCall = async (cardId) => {
  const url = `https://api.trello.com/1/cards/${cardId}?key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A`;
  const response = await axios({
    method: "delete",
    url: url,
  });
  return response.data;
};

export const getChecklists = async (cardId) => {
  const url = `https://api.trello.com/1/cards/${cardId}/checklists?key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A`;
  const response = await axios.get(url);
  return response.data;
};

export const createChecklistApiCall = async (cardId, checklistName) => {
  const url = `https://api.trello.com/1/checklists?idCard=${cardId}&key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A&name=${checklistName}`;
  const response = await axios({
    method: "post",
    url: url,
  });
  return response.data;
};

export const deleteChecklistApiCall = async (checklistId) => {
  const url = `https://api.trello.com/1/checklists/${checklistId}?key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A`;
  const response = await axios({
    method: "delete",
    url: url,
  });
  return response;
};

export const createCheckItemApiCall = async (checklistId, checkItemName) => {
  const url = `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${checkItemName}&key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A`;
  const response = await axios({
    method: "post",
    url: url,
  });
  return response.data;
};

export const toggleCheckBoxApiCall = async (cardId, checkItemId, state) => {
  const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A&state=${state}`;
  const response = await axios({
    method: "put",
    url: url,
  });
  return response.data;
};

export const deleteCheckItemApiCall = async (checklistId, checkItemId) => {
  const url = `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=81534b6aebcf833336e6fefdf0bf8f01&token=ATTA22a138049d2f1ae49360729bab3d7fd587aa4d9c359a2ec13e9a505f0e6c6aaeF5B1DB6A`;
  const response = await axios({
    method: "delete",
    url: url,
  });
  return response;
};
