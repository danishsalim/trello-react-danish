import React from "react";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Lists from "./pages/Lists";
import Header from "./components/Header";
import NotFound from "./pages/NotFound";
import "./App.css";
import { Toaster } from "react-hot-toast";

const App = () => {
  return (
    <>
      <Header />
      <Toaster/>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/boards/:id" element={<Lists />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </>
  );
};

export default App;
